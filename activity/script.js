let trainer={
	name:"Ash Ketchum",
	age:10,
	friends:{
		hoenn:["May","Max"],
		kanto:["Brock","Misty"]
	},
	pokemon:["Pikachu","Charizard","Squirtle","Bulbasaur"],
	talk:function(){
		console.log("Pikachu, I choose you!")
	}
}
console.log(trainer)
console.log("Result of dot notation:")
console.log(trainer.name)
console.log("Result of square bracket notation:")
console.log(trainer.pokemon)
console.log("Result of talk method:")
console.log(trainer.talk())

let pokemon = {
	name: "Totodile",
	level: 14,
	health: 70,
	attack: 42,
	tackle: function (){
		console.log(pokemon.name+ " tackled " +targetPokemon)
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
}

function Pokemon(name, level){
	this.name=name,
	this.level=level,
	this.health=5*level,
	this.attack=3*level
	this.tackle = function(target){
		let targetHealth=target.health-this.attack
		console.log(this.name + " tackled " + target.name);
		console.log(target.name+"'s health is now reduced to "+targetHealth);
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let Arcanine=new Pokemon("Arcanine", 30)
let Suicune=new Pokemon("Suicune", 40)
let Blaziken=new Pokemon("Blaziken", 45)

console.log(Arcanine)
console.log(Suicune)
console.log(Blaziken)

Arcanine.tackle(Suicune)




